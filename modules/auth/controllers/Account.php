<?php defined('BASEPATH') or exit('No direct script access allowed');

class Account extends RestController
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('members');
        $this->load->model('member_otp');
        $this->load->model('member_tokens');

        $this->return = [ 
            'status' => false,
            // 'message' => ''
        ];
        $this->status_code = 400;
        $this->error = '';

        $this->validate_keys();
    }

    public function list_post()
    {
        header("Expires: Mon, 11 Apr 1988 05:52:00 GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");

        $post_data = json_decode($this->input->raw_input_stream);

        $patient_all = $this->members->get_all(['member_id'=>$post_data->member_id]);
        $this->return['patients'] = $patient_all;
        $this->return['status']   = true;
        $this->status_code        = 200;

        $this->response( $this->return, $this->status_code );
    }

    public function add_post()
    {
        header("Expires: Mon, 11 Apr 1988 05:52:00 GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");

        $post_data = json_decode($this->input->raw_input_stream);

        // cek nama
        // apakah sudah ada atau belum
        $check_name = $this->members->get(['name'=>$post_data->name]);
        if($check_name) {
            $this->return['message'] = 'Nama pasien sudah terdaftar.';
        } else {

            // simpan data pasien baru
            $save_patient = $this->members->insert([
                'member_id' => $post_data->member_id, 
                'name'      => $post_data->name
            ]);

            if($save_patient) {
                $this->return['status']  = true;
                $this->return['message'] = 'Data pasien baru berhasil ditambahkan.';
                $this->status_code       = 200;
            } else {
                $this->return['message'] = 'Terjadi kesalahan ketika menyimpan data, silahkan dicoba kembali.';
            }
        }

        $this->response( $this->return, $this->status_code );
    }

    public function edit_post()
    {
        header("Expires: Mon, 11 Apr 1988 05:52:00 GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");

        $post_data = json_decode($this->input->raw_input_stream);

        $patient = $this->members->get($post_data->id);
        $this->return['patient'] = $patient;
        $this->return['status']  = true;
        $this->status_code       = 200;

        $this->response( $this->return, $this->status_code );
    }

    public function save_post()
    {
        header("Expires: Mon, 11 Apr 1988 05:52:00 GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");

        $post_data = json_decode($this->input->raw_input_stream);

        $check_name = $this->members->get(['id != '=>$post_data->id, 'name'=>$post_data->name]);
        if($check_name) {
            $this->return['message'] = 'Nama pasien sudah terdaftar.';
        } else {
            // update data pasien
            $update_patient = $this->members->update([
                'name' => $post_data->name
            ], $post_data->id);

            if($update_patient) {
                $this->return['status']  = true;
                $this->return['message'] = 'Data Pasien berhasil disimpan.';
                $this->status_code       = 200;
            } else {
                $this->return['message'] = 'Terjadi kesalahan ketika menyimpan data, silahkan dicoba kembali.';
            }
        }

        $this->response( $this->return, $this->status_code );
    }

}
