<?php defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends RestController
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('users');
        $this->load->model('user_tokens');

        $this->return = [ 
            'status' => false,
            'message' => ''
        ];
        $this->status_code = 200;
        $this->error = '';
    }

    public function login_post()
    {
        header("Expires: Mon, 11 Apr 1988 05:52:00 GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");

        $post_data = json_decode($this->input->raw_input_stream);

        // cek inputan username dan password
        // inputan tidak boleh kosong
        if ($post_data->username == '') {
            $this->error .= 'Username harus diisi';
        } elseif ($post_data->password == '') {
            $this->error .= 'Password harus diisi';
        }

        if ($this->error == '') {
            $user = $this->check_user($post_data->username);
            if ($user) {
                $verify = $this->verify($user, $post_data->password);
                if($verify) {
                    $token = $this->set_token($user);
                    if($token !== false) {
                        $this->return['results'] = [
                            'token' => $token,
                            'user_id' => encode($user->id),
                            'user_name' => $user->name,
                            'group_id' => $user->group_id,
                            'group_name' => ARRAY_GROUP[$user->group_id],
                        ];
                        $this->return['status'] = true;
                        $this->return['message'] = 'Login berhasil.';
                    } else {
                        $this->return['message'] = 'Login gagal.';
                    }
                } else {
                    $this->return['message'] = 'Password anda salah.';
                }
            } else {
                $this->return['message'] = 'Username tidak terdaftar.';
            }
        } else {
            $this->return['message'] = $this->error;
        }

        $this->response( $this->return, $this->status_code );
    }

    public function logout_post()
    {
        header("Expires: Mon, 11 Apr 1988 05:52:00 GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");

        $post_data = json_decode($this->input->raw_input_stream);

        if ($post_data->token == '') {
            $this->error .= 'Token not found';
        }

        if ($this->error == '') {
            $this->user_keys->delete(['key' => $post_data->token]);
            $this->return['status'] = true;
            $this->return['message'] = 'Logged out successfully';
            $this->status_code = 200;
        } else {
            $this->return['message'] = $this->error;
        }

        $this->response( $this->return, $this->status_code );
    }

    private function check_user($username)
    {
        $user = $this->users->get([
            'username' => $username
        ]);
        return $user ? $user : false;
    }

    private function verify($user, $password)
    {
        $this->load->library('password');
        $verify = $this->password->check_password($password, $user->password);

        if ($verify) {
            $this->users->update(['last_login' => date('Y-m-d H:i:s')], $user->id);
            return true;
        }
        return false;
    }

    private function set_token($user)
    {
        $token = sha1($user->id.':'.$user->username.':'.time().uniqid());
        $this->user_tokens->delete(['user_id'=>$user->id]);
        $save_token = $this->user_tokens->insert([
            'user_id' => $user->id,
            'token' => $token,
        ]);
        return $save_token ? $token : false;
    }

    public function hash($password)
    {
        $this->load->library('password');
        echo $this->password->hash_password($password);
        exit;
    }
}
