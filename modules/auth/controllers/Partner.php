<?php defined('BASEPATH') or exit('No direct script access allowed');

class Partner extends RestController
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('partners');
        $this->load->model('partner_otp');
        $this->load->model('partner_tokens');

        $this->return = [ 
            'status' => false,
            // 'message' => ''
        ];
        $this->status_code = 400;
        $this->error = '';
    }

    public function register_post()
    {
        header("Expires: Mon, 11 Apr 1988 05:52:00 GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");

        $post_data = json_decode($this->input->raw_input_stream);

        // cek register
        // cek email dan No. HP
        $check_register = $this->partners->check_register($post_data);
        if($check_register) {
            $this->return['message'] = 'Email atau No HP anda sudah terdaftar, silahkan login untuk melanjutkan.';
        } else {
            $str = isset($post_data->str) ? $post_data->str : '';

            $register = $this->partners->insert([
                'type_id' => $post_data->type_id,
                'category_id' => $post_data->category_id,
                'name'  => $post_data->name,
                'email' => $post_data->email,
                'phone' => $post_data->phone,
                'str'   => $str
            ]);
            if ($register) {
                $this->return['status'] = true;
                $this->status_code = 200;
            }
        }

        $this->response( $this->return, $this->status_code );
    }

    public function request_otp_post()
    {
        header("Expires: Mon, 11 Apr 1988 05:52:00 GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");

        $post_data = json_decode($this->input->raw_input_stream);

        // cek nomor hp 
        // sudah mendaftar atau belum
        $check_phone = $this->partners->get(['phone'=>$post_data->phone]);
        if(!$check_phone) {
            $this->return['message'] = 'Nomor HP yang anda masukkan belum terdaftar, silahkan daftar terlebih dahulu.';
        } else {
            // request ke api sms
            // untuk ambil kode otp dari api sms
                /* { source request ke api sms... } */
            $otp = rand(1000,9999);

            // simpan kode opt ke table partner_otp
            $partner_id = $check_phone->id;
            $save_otp = $this->partner_otp->insert([
                'partner_id' => $partner_id, 
                'code'       => $otp
            ]);

            if($save_otp) {
                $this->return['status'] = true;
                $this->status_code = 200;
            } else {
                $this->return['message'] = 'Permintaan kode OTP gagal, silahkan dicoba kembali.';
            }
        }

        $this->response( $this->return, $this->status_code );
    }

    public function validate_otp_post()
    {
        header("Expires: Mon, 11 Apr 1988 05:52:00 GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");

        $post_data = json_decode($this->input->raw_input_stream);

        // cek otp
        $check_otp = $this->partner_otp->check_otp($post_data->otp);

        if(!$check_otp) {
            $this->return['message'] = 'Kode OTP yang anda masukkan salah, silahkan dicoba kembali.';
        } else {
            // cek kadaluarsa OTP
            $otp_created_at = $check_otp->otp_created_at;
            $date_now       = date('Y-m-d H:i:s');
            $time           = duration_time($otp_created_at, $date_now);

            if($time > 120) {
                // hapus OTP
                $this->partner_otp->delete($check_otp->partner_otp_id);
                
                $this->return['message'] = 'Kode OTP sudah kadaluarsa, silahkan login kembali.';
            } else {
                // buat token
                $token = $this->set_token($check_otp);

                if($token == false) {
                    $this->return['message'] = 'Terjadi kesalahan server, silahkan masukkan kembali kode OTP.';
                } else {
                    // hapus OTP
                    $this->partner_otp->delete($check_otp->partner_otp_id);

                    $this->return['user'] = [
                        'token' => $token,
                        'partner_category' => $check_otp->partner_category,
                        'partner_type' => $check_otp->partner_type,
                        'name'  => $check_otp->name,
                        'name'  => $check_otp->name,
                        'email' => $check_otp->email,
                        'phone' => $check_otp->phone
                    ];
                    $this->return['status'] = true;
                    $this->status_code = 200;
                }

            }
        }

        $this->response( $this->return, $this->status_code );
    }

    private function set_token($partner)
    {
        $token = sha1($partner->id.':'.$partner->email.':'.time().uniqid());
        $create_token = $this->partner_tokens->insert([
            'partner_id' => $partner->id,
            'token' => $token,
        ]);
        return $create_token ? $token : false;
    }
}
