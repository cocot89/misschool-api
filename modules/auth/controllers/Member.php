<?php defined('BASEPATH') or exit('No direct script access allowed');

class Member extends RestController
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('members');
        $this->load->model('member_otp');
        $this->load->model('member_tokens');

        $this->return = [
            'status' => false,
            // 'message' => ''
        ];
        $this->status_code = 400;
        $this->error = '';
    }

    public function register_post()
    {
        $post_data = json_decode($this->input->raw_input_stream);

        // cek register
        // cek email dan no. hp
        $post_data->phone = intval($post_data->phone);

        $check_register = $this->members->check_register($post_data);
        if ($check_register) {
            $member = $check_register;
            if ($member->status == 'pending') {
                $update_data = $this->members->update([
                    'name'  => $post_data->name,
                    'email' => $post_data->email,
                    'phone' => $post_data->phone,
                ], $member->id);
                if ($update_data) {
                    $this->return['status'] = true;
                    $this->status_code = 200;
                }
            } else {
                $this->return['message'] = 'Email atau No HP anda sudah terdaftar, silahkan login untuk melanjutkan.';
            }
        } else {
            $register = $this->members->insert([
                'name'  => $post_data->name,
                'email' => $post_data->email,
                'phone' => $post_data->phone
            ]);
            if ($register) {
                $this->return['status'] = true;
                $this->status_code = 200;
            }
        }

        $this->response($this->return, $this->status_code);
    }

    public function facebook_post()
    {
        $post_data = json_decode($this->input->raw_input_stream);

        $member_data = [
            'name'  => $post_data->name,
            'email' => $post_data->email,
            'picture' => $post_data->picture,
            'facebook_id' => $post_data->facebook_id,
            'status' => 'active'
        ];

        $member = $this->members->fields('id,name,email,phone')->get(['email' => $post_data->email]);
        if ($member) {
            $this->members->update($member_data, $member->id);

            $this->return['user'] = $this->set_token($member, $post_data->user_agent);
            $this->return['status'] = true;
            $this->status_code = 200;
        } else {
            $register = $this->members->insert($member_data);
            if ($register) {
                $member = $this->members->fields('id,name,email,phone')->get($register);

                $this->return['user'] = $this->set_token($member, $post_data->user_agent);
                $this->return['status'] = true;
                $this->status_code = 200;
            }
        }

        $this->response($this->return, $this->status_code);
    }

    public function google_post()
    {
        $post_data = json_decode($this->input->raw_input_stream);

        $member_data = [
            'name'  => $post_data->name,
            'email' => $post_data->email,
            'picture' => $post_data->picture,
            'google_id' => $post_data->google_id,
            'status' => 'active'
        ];

        $member = $this->members->fields('id,name,email,phone')->get(['email' => $post_data->email]);
        if ($member) {
            $this->members->update($member_data, $member->id);

            $this->return['user'] = $this->set_token($member, $post_data->user_agent);
            $this->return['status'] = true;
            $this->status_code = 200;
        } else {
            $register = $this->members->insert($member_data);
            if ($register) {
                $member = $this->members->fields('id,name,email,phone')->get($register);

                $this->return['user'] = $this->set_token($member, $post_data->user_agent);
                $this->return['status'] = true;
                $this->status_code = 200;
            }
        }

        $this->response($this->return, $this->status_code);
    }

    public function request_otp_post()
    {
        $this->load->library('sms');

        $post_data = json_decode($this->input->raw_input_stream);
        $post_data->phone = intval($post_data->phone);

        // cek nomor hp
        // sudah mendaftar atau belum
        $check_phone = $this->members->get(['phone' => $post_data->phone]);
        if (!$check_phone) {
            $this->return['message'] = 'Nomor HP yang anda masukkan belum terdaftar, silahkan daftar terlebih dahulu.';
        } else {
            // request ke api sms
            // untuk ambil kode otp dari api sms
            /* { source request ke api sms... } */
            $otp = rand(1000, 9999);
            $message = 'Kode OTP E+Meds '.$otp;
            $send_otp = $this->sms->send($check_phone->phone, $message);

            if ($send_otp) {
                $member_id = $check_phone->id;
                // simpan kode opt ke table member_otp
                $save_otp = $this->member_otp->insert([
                    'member_id' => $member_id,
                    'otp'       => $otp
                ]);
                $this->return['status'] = true;
                $this->status_code = 200;
            } else {
                $this->return['message'] = 'Permintaan kode OTP gagal, silahkan dicoba kembali.';
            }
        }

        $this->response($this->return, $this->status_code);
    }

    public function validate_otp_post()
    {
        $post_data = json_decode($this->input->raw_input_stream);

        // cek otp
        $post_data->phone = intval($post_data->phone);
        $check_otp = $this->member_otp->check_otp($post_data->phone, $post_data->otp);

        if (!$check_otp) {
            $this->return['message'] = 'Kode OTP yang anda masukkan salah, silahkan dicoba kembali.';
        } else {
            // cek kadaluarsa OTP
            $otp_created_at = $check_otp->otp_created_at;
            $date_now       = date('Y-m-d H:i:s');
            $time           = duration_time($otp_created_at, $date_now);

            if ($time > 120) {
                // hapus OTP
                $this->member_otp->delete($check_otp->member_otp_id);
                
                $this->return['message'] = 'Kode OTP sudah kadaluarsa, silahkan login kembali.';
            } else {
                // buat token
                $token = $this->set_token($check_otp, $post_data->user_agent);

                if ($token == false) {
                    $this->return['message'] = 'Terjadi kesalahan server, silahkan masukkan kembali kode OTP.';
                } else {
                    $member = $check_otp;
                    if ($member->status == 'pending') {
                        $this->members->update(['status'=>'active'], $member->id);
                    }

                    // hapus OTP
                    $this->member_otp->delete($check_otp->member_otp_id);

                    $this->return['user'] = $token;
                    $this->return['status'] = true;
                    $this->status_code = 200;
                }
            }
        }

        $this->response($this->return, $this->status_code);
    }

    private function set_token($member, $user_agent)
    {
        $token = sha1($member->id.':'.$member->email.':'.time().uniqid());
        $create_token = $this->member_tokens->insert([
            'member_id' => $member->id,
            'token' => $token,
            'user_agent' => $user_agent
        ]);
        return $create_token ? [
                    'token' => $token,
                    'name'  => $member->name,
                    'email' => $member->email,
                    'phone' => $member->phone
                ] : false;
    }
}
