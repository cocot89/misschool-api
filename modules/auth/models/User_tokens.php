<?php defined('BASEPATH') or exit('No direct script access allowed');

class User_tokens extends MY_Model
{
    public $table = 'users_tokens';
    public $primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->soft_deletes = false;
    }

    public function check_token($user_id, $token)
    {
        $this->db->select('users.*');
        $this->db->join('users', 'users_tokens.user_id = users.id', 'inner');
        $this->db->where('users_tokens.token', $token);
        $this->db->where('users_tokens.user_id', $user_id);
        $query = $this->db->get($this->table);
        
        return ($query) ? $query->row() : false;
    }
}
