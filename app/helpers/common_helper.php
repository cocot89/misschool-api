<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Jiwalu Framework
 * A framework for PHP development
 *
 * @package     Jiwalu Framework
 * @author      Jiwalu Studio
 * @copyright   Copyright (c) 2019, Jiwalu Studio (http://www.jiwalu.id)
 */
if (!function_exists('check_token')) {

    function check_token($token) {
        $app = & get_instance();
        $app->load->model('member/member_tokens');
        $user = $app->member_tokens->check_token($token);
        if ($user) {
            return $user;
        }
        return false;
    }

}
if (!function_exists('mobile_token')) {

    function mobile_token($token) {
        $app = & get_instance();
        $app->load->model('master/drivers');
        $user = $app->drivers->get(['uid' => $token]);
        if ($user) {
            return $user;
        }
        return false;
    }

}

if (!function_exists('encode')) {

    function encode($string) {
        return encrypt_decrypt('encrypt', $string);
    }

}

if (!function_exists('decode')) {

    function decode($string) {
        return encrypt_decrypt('decrypt', $string);
    }

}

if (!function_exists('encrypt_decrypt')) {

    function encrypt_decrypt($action, $string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = '4y0b3b@sk4nD!R1d4r1r!b4';
        $secret_iv = 'h1DupT4np@r!b@';

        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }

}

/* ==-- JSON to Array --== */
if (!function_exists('to_array')) {

    function to_array($json) {
        $array = json_decode($json, TRUE);
        return (is_array($array)) ? $array : FALSE;
    }

}

/* ==-- Array to JSON --== */
if (!function_exists('to_json')) {

    function to_json($array) {
        return (is_array($array)) ? json_encode($array) : FALSE;
    }

}

/* ==-- Age calculator --== */
if (!function_exists('age')) {

    function age($birthdate) {
        $from = new DateTime($birthdate);
        $to = new DateTime('today');
        return $from->diff($to)->y;
    }

}

/* ==-- Ajax redirect --== */
if (!function_exists('ajaxRedirect')) {

    function ajaxRedirect($redirect = '', $timer = 10000) {
        if ($timer == 0) {
            return '<script>window.location.href="' . site_url($redirect) . '";</script>';
        } else {
            return '<script>setTimeout("window.location.href=\'' . site_url($redirect) . '\'",' . $timer . ');</script>';
        }
    }

}

if (!function_exists('date_indo')) {

    function date_indo($fulldate) {
        $date = substr($fulldate, 8, 2);
        $month = get_month(substr($fulldate, 5, 2));
        $year = substr($fulldate, 0, 4);
        return $date . ' ' . $month . ' ' . $year;
    }

}

if (!function_exists('date_simple')) {

    function date_simple($fulldate) {
        $date = substr($fulldate, 8, 2);
        $month = substr($fulldate, 5, 2);
        $year = substr($fulldate, 0, 4);
        return $date . '/' . $month . '/' . $year;
    }

}

if (!function_exists('date_normal')) {

    function date_normal($fulldate) {
        $date = substr($fulldate, 8, 2);
        $month = get_month3(substr($fulldate, 5, 2));
        $year = substr($fulldate, 0, 4);
        $time = substr($fulldate, 11, 8);
        return $date . '/' . $month . '/' . $year . ' ' . $time;
    }

}

if (!function_exists('date_time')) {

    function date_time($fulldate) {
        $date = substr($fulldate, 0, 2);
        $month = get_month2(substr($fulldate, 3, 3));
        $year = substr($fulldate, 7, 4);
        $time = substr($fulldate, 12, 5);
        return $year . '-' . $month . '-' . $date . ' ' . $time;
    }

}

if (!function_exists('mysql_date')) {

    function mysql_date($fulldate) {
        $date = substr($fulldate, 0, 2);
        $month = substr($fulldate, 3, 2);
        $year = substr($fulldate, 6, 4);
        $time = substr($fulldate, 11, 8);
        return $year . '-' . $month . '-' . $date . ' ' . $time;
    }

}

if (!function_exists('get_month')) {

    function get_month($month) {
        switch ($month) {
            case 1: return "Januari";
            case 2: return "Februari";
            case 3: return "Maret";
            case 4: return "April";
            case 5: return "Mei";
            case 6: return "Juni";
            case 7: return "Juli";
            case 8: return "Agustus";
            case 9: return "September";
            case 10: return "Oktober";
            case 11: return "November";
            case 12: return "Desember";
        }
    }

}

if (!function_exists('get_month2')) {

    function get_month2($month) {
        switch ($month) {
            case "Jan": return "01";
            case "Feb": return "02";
            case "Mar": return "03";
            case "Apr": return "04";
            case "May": return "05";
            case "Jun": return "06";
            case "Jul": return "07";
            case "Aug": return "08";
            case "Sep": return "09";
            case "Oct": return "10";
            case "Nov": return "11";
            case "Dec": return "12";
        }
    }

}

if (!function_exists('get_month3')) {

    function get_month3($month) {
        switch ($month) {
            case "01": return "Jan";
            case "02": return "Feb";
            case "03": return "Mar";
            case "04": return "Apr";
            case "05": return "May";
            case "06": return "Jun";
            case "07": return "Jul";
            case "08": return "Aug";
            case "09": return "Sep";
            case "10": return "Oct";
            case "11": return "Nov";
            case "12": return "Dec";
        }
    }

}

if (!function_exists('duration')) {

    function duration($time) {
        $diff = $time / 1000;

        if ($diff < 1) {
            return 'less than 1 sec';
        }

        $time_rules = array(
            12 * 30 * 24 * 60 * 60 => 'year',
            30 * 24 * 60 * 60 => 'month',
            24 * 60 * 60 => 'day',
            60 * 60 => 'hour',
            60 => 'min',
            1 => 'sec'
        );

        foreach ($time_rules as $secs => $str) {
            $div = $diff / $secs;
            if ($div >= 1) {
                $t = round($div);
                return $t . ' ' . $str .
                        ( $t > 1 ? 's' : '' );
            }
        }
    }

}

if (!function_exists('format_number')) {

    function format_number($number, $decimal = 0) {
        $number = round($number, $decimal);
        return number_format($number, $decimal, ',', '.');
    }

}

if (!function_exists('array_unshift_assoc')) {

    function array_unshift_assoc(&$arr, $key, $val) {
        $arr = array_reverse($arr, true);
        $arr[$key] = $val;
        $arr = array_reverse($arr, true);
        return $arr;
    }

}

if (!function_exists('work_hours')) {

    function work_hours($start, $end) {
        $return = ['minute' => 0, 'hour' => 0];
        if ($start && $end) {
            $start = strtotime($start);
            $end = strtotime($end);
            $return['minute'] = round(($end - $start) / 60, 2);
            $return['hour'] = round($return['minute'] / 60, 2);
        }
        return $return;
    }

}

if (!function_exists('duration_time')) {
    function duration_time($date_in = '', $date_out = '') {
        $from = new DateTime($date_in);
        $to   = new DateTime($date_out);
        $d = $from->diff($to)->d * 24 * 60 * 60;
        $h = $from->diff($to)->h * 60 * 60;
        $i = $from->diff($to)->i * 60;
        $s = $from->diff($to)->s;
        $duration_time = intval($d) + intval($h) + intval($i) + intval($s);
        return $duration_time;
    }
}
/* End of file common_helper.php */
/* Location: ./system/helpers/common_helper.php */
