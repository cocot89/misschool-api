<?php

$lang['delivery'] = "Delivery";
$lang['delivery_work_order'] = "Delivery Work Order";
$lang['delivery_cycle'] = "Delivery Cycle";
$lang['delivery_plan'] = "Delivery Plan";
$lang['driver'] = "Driver";
$lang['route'] = "Route";
$lang['location'] = "Location";

$lang['insert_success_message'] = "%s has been successfully added.";
$lang['update_success_message'] = "%s has been successfully updated.";
$lang['delete_success_message'] = "%s has been successfully deleted.";
$lang['delete_error_message'] = "%s was not deleted. Please contact system administrator.";
$lang['empty_trash_success_message'] = "Trash sucessfully emptied.";
$lang['empty_trash_error_message'] = "Failed to emptying trash.";
$lang['restore_success_message'] = "Data sucessfully restored.";
$lang['restore_error_message'] = "Failed to restore data.";
$lang['remove_success_message'] = "%s has been moved to trash.";
$lang['not_found_message'] = "Data not found.";
$lang['not_authorization_message'] = "You are not authorized.";