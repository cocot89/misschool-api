<button class="kt-aside-close" id="kt_aside_close_btn"><i class="la la-close"></i></button>
<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">
  <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
    <div id="kt_aside_menu" class="kt-aside-menu" data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
      <ul class="kt-menu__nav">
        <li class="kt-menu__item" aria-haspopup="true">
          <a href="<?php echo site_url(); ?>" class="kt-menu__link">
            <i class="kt-menu__link-icon flaticon2-protection"></i><span class="kt-menu__link-text">Dashboard</span>
          </a>
        </li>
        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
          <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
            <i class="kt-menu__link-icon flaticon2-list-3"></i><span class="kt-menu__link-text">Master Data</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
          </a>
          <div class="kt-menu__submenu"><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
              <li class="kt-menu__item" aria-haspopup="true">
                <a href="<?php echo site_url('master/division'); ?>" class="kt-menu__link">
                  <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Manage Division</span>
                </a>
              </li>
              <li class="kt-menu__item" aria-haspopup="true">
                <a href="<?php echo site_url('master/department'); ?>" class="kt-menu__link">
                  <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Manage Department</span>
                </a>
              </li>
              <li class="kt-menu__item" aria-haspopup="true">
                <a href="<?php echo site_url('master/position'); ?>" class="kt-menu__link">
                  <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Manage Position</span>
                </a>
              </li>
              <li class="kt-menu__item" aria-haspopup="true">
                <a href="<?php echo site_url('master/jobtitle'); ?>" class="kt-menu__link">
                  <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Manage Job Title</span>
                </a>
              </li>
              <li class="kt-menu__item" aria-haspopup="true">
                <a href="<?php echo site_url('master/branch'); ?>" class="kt-menu__link">
                  <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Manage Branch</span>
                </a>
              </li>
              <li class="kt-menu__item" aria-haspopup="true">
                <a href="<?php echo site_url('master/employee'); ?>" class="kt-menu__link">
                  <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Manage Employee</span>
                </a>
              </li>
            </ul>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>