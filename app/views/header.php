<!-- begin:: Header -->
<div id="kt_header" class="kt-header kt-grid kt-grid--ver  kt-header--fixed ">

	<!-- begin:: Aside -->
	<div class="kt-header__brand kt-grid__item  " id="kt_header_brand">
		<div class="kt-header__brand-logo">
			<a href="<?php echo site_url(); ?>">
				<img alt="Logo" src="<?php echo base_url(); ?>assets/media/favicon.png" style="width:60px">
			</a>
		</div>
	</div>

	<!-- end:: Aside -->

	<h3 class="kt-header__title kt-grid__item">
		<?php echo userinfo('company_name'); ?>
	</h3>

	<!-- end:: Title -->

	<!-- begin:: Header Topbar -->
	<div class="kt-header__topbar">
		<!--begin: Notifications -->
		<div class="kt-header__topbar-item dropdown">
			<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
				<span class="kt-header__topbar-icon kt-header__topbar-icon--success"><i class="flaticon2-bell-alarm-symbol"></i></span>
				<span class="kt-hidden kt-badge kt-badge--danger"></span>
			</div>
			<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
				<form>

					<!--begin: Head -->
					<div class="kt-head kt-head--skin-dark kt-head--fit-x kt-head--fit-b" style="background-image: url(<?php echo base_url(); ?>assets/media/misc/bg-1.jpg)">
						<h3 class="kt-head__title">
							User Notifications
							&nbsp;
							<span class="btn btn-success btn-sm btn-bold btn-font-md">23 new</span>
						</h3>
						<ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-success kt-notification-item-padding-x" role="tablist">
							<li class="nav-item">
								<a class="nav-link active show" data-toggle="tab" href="#topbar_notifications_notifications" role="tab" aria-selected="true">Alerts</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#topbar_notifications_events" role="tab" aria-selected="false">Events</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#topbar_notifications_logs" role="tab" aria-selected="false">Logs</a>
							</li>
						</ul>
					</div>

					<!--end: Head -->
					<div class="tab-content">
						<div class="tab-pane active show" id="topbar_notifications_notifications" role="tabpanel">
							<div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
								<a href="#" class="kt-notification__item">
									<div class="kt-notification__item-icon">
										<i class="flaticon2-line-chart kt-font-success"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											New order has been received
										</div>
										<div class="kt-notification__item-time">
											2 hrs ago
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification__item">
									<div class="kt-notification__item-icon">
										<i class="flaticon2-box-1 kt-font-brand"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											New customer is registered
										</div>
										<div class="kt-notification__item-time">
											3 hrs ago
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification__item">
									<div class="kt-notification__item-icon">
										<i class="flaticon2-chart2 kt-font-danger"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											Application has been approved
										</div>
										<div class="kt-notification__item-time">
											3 hrs ago
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification__item">
									<div class="kt-notification__item-icon">
										<i class="flaticon2-image-file kt-font-warning"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											New file has been uploaded
										</div>
										<div class="kt-notification__item-time">
											5 hrs ago
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification__item">
									<div class="kt-notification__item-icon">
										<i class="flaticon2-drop kt-font-info"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											New user feedback received
										</div>
										<div class="kt-notification__item-time">
											8 hrs ago
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification__item">
									<div class="kt-notification__item-icon">
										<i class="flaticon2-pie-chart-2 kt-font-success"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											System reboot has been successfully completed
										</div>
										<div class="kt-notification__item-time">
											12 hrs ago
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification__item">
									<div class="kt-notification__item-icon">
										<i class="flaticon2-favourite kt-font-danger"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											New order has been placed
										</div>
										<div class="kt-notification__item-time">
											15 hrs ago
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification__item kt-notification__item--read">
									<div class="kt-notification__item-icon">
										<i class="flaticon2-safe kt-font-primary"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											Company meeting canceled
										</div>
										<div class="kt-notification__item-time">
											19 hrs ago
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification__item">
									<div class="kt-notification__item-icon">
										<i class="flaticon2-psd kt-font-success"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											New report has been received
										</div>
										<div class="kt-notification__item-time">
											23 hrs ago
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification__item">
									<div class="kt-notification__item-icon">
										<i class="flaticon-download-1 kt-font-danger"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											Finance report has been generated
										</div>
										<div class="kt-notification__item-time">
											25 hrs ago
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification__item">
									<div class="kt-notification__item-icon">
										<i class="flaticon-security kt-font-warning"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											New customer comment recieved
										</div>
										<div class="kt-notification__item-time">
											2 days ago
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification__item">
									<div class="kt-notification__item-icon">
										<i class="flaticon2-pie-chart kt-font-success"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											New customer is registered
										</div>
										<div class="kt-notification__item-time">
											3 days ago
										</div>
									</div>
								</a>
							</div>
						</div>
						<div class="tab-pane" id="topbar_notifications_events" role="tabpanel">
							<div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
								<a href="#" class="kt-notification__item">
									<div class="kt-notification__item-icon">
										<i class="flaticon2-psd kt-font-success"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											New report has been received
										</div>
										<div class="kt-notification__item-time">
											23 hrs ago
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification__item">
									<div class="kt-notification__item-icon">
										<i class="flaticon-download-1 kt-font-danger"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											Finance report has been generated
										</div>
										<div class="kt-notification__item-time">
											25 hrs ago
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification__item">
									<div class="kt-notification__item-icon">
										<i class="flaticon2-line-chart kt-font-success"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											New order has been received
										</div>
										<div class="kt-notification__item-time">
											2 hrs ago
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification__item">
									<div class="kt-notification__item-icon">
										<i class="flaticon2-box-1 kt-font-brand"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											New customer is registered
										</div>
										<div class="kt-notification__item-time">
											3 hrs ago
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification__item">
									<div class="kt-notification__item-icon">
										<i class="flaticon2-chart2 kt-font-danger"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											Application has been approved
										</div>
										<div class="kt-notification__item-time">
											3 hrs ago
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification__item">
									<div class="kt-notification__item-icon">
										<i class="flaticon2-image-file kt-font-warning"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											New file has been uploaded
										</div>
										<div class="kt-notification__item-time">
											5 hrs ago
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification__item">
									<div class="kt-notification__item-icon">
										<i class="flaticon2-drop kt-font-info"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											New user feedback received
										</div>
										<div class="kt-notification__item-time">
											8 hrs ago
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification__item">
									<div class="kt-notification__item-icon">
										<i class="flaticon2-pie-chart-2 kt-font-success"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											System reboot has been successfully completed
										</div>
										<div class="kt-notification__item-time">
											12 hrs ago
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification__item">
									<div class="kt-notification__item-icon">
										<i class="flaticon2-favourite kt-font-brand"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											New order has been placed
										</div>
										<div class="kt-notification__item-time">
											15 hrs ago
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification__item kt-notification__item--read">
									<div class="kt-notification__item-icon">
										<i class="flaticon2-safe kt-font-primary"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											Company meeting canceled
										</div>
										<div class="kt-notification__item-time">
											19 hrs ago
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification__item">
									<div class="kt-notification__item-icon">
										<i class="flaticon2-psd kt-font-success"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											New report has been received
										</div>
										<div class="kt-notification__item-time">
											23 hrs ago
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification__item">
									<div class="kt-notification__item-icon">
										<i class="flaticon-download-1 kt-font-danger"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											Finance report has been generated
										</div>
										<div class="kt-notification__item-time">
											25 hrs ago
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification__item">
									<div class="kt-notification__item-icon">
										<i class="flaticon-security kt-font-warning"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											New customer comment recieved
										</div>
										<div class="kt-notification__item-time">
											2 days ago
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification__item">
									<div class="kt-notification__item-icon">
										<i class="flaticon2-pie-chart kt-font-success"></i>
									</div>
									<div class="kt-notification__item-details">
										<div class="kt-notification__item-title">
											New customer is registered
										</div>
										<div class="kt-notification__item-time">
											3 days ago
										</div>
									</div>
								</a>
							</div>
						</div>
						<div class="tab-pane" id="topbar_notifications_logs" role="tabpanel">
							<div class="kt-grid kt-grid--ver" style="min-height: 200px;">
								<div class="kt-grid kt-grid--hor kt-grid__item kt-grid__item--fluid kt-grid__item--middle">
									<div class="kt-grid__item kt-grid__item--middle kt-align-center">
										All caught up!
										<br>No new notifications.
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>

		<!--end: Notifications -->

		<!--begin: Language bar -->
		<div class="kt-header__topbar-item kt-header__topbar-item--langs">
			<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
				<span class="kt-header__topbar-icon kt-header__topbar-icon--brand">
					<img class="" src="<?php echo base_url(); ?>assets/media/flags/260-united-kingdom.svg" alt="" />
				</span>
			</div>
			<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim">
				<ul class="kt-nav kt-margin-t-10 kt-margin-b-10">
					<li class="kt-nav__item kt-nav__item--active">
						<a href="#" class="kt-nav__link">
							<span class="kt-nav__link-icon"><img src="<?php echo base_url(); ?>assets/media/flags/226-united-states.svg" alt="" /></span>
							<span class="kt-nav__link-text">English</span>
						</a>
					</li>
					<li class="kt-nav__item">
						<a href="#" class="kt-nav__link">
							<span class="kt-nav__link-icon"><img src="<?php echo base_url(); ?>assets/media/flags/128-spain.svg" alt="" /></span>
							<span class="kt-nav__link-text">Spanish</span>
						</a>
					</li>
					<li class="kt-nav__item">
						<a href="#" class="kt-nav__link">
							<span class="kt-nav__link-icon"><img src="<?php echo base_url(); ?>assets/media/flags/162-germany.svg" alt="" /></span>
							<span class="kt-nav__link-text">German</span>
						</a>
					</li>
				</ul>
			</div>
		</div>

		<!--end: Language bar -->

		<!--begin: User bar -->
		<div class="kt-header__topbar-item kt-header__topbar-item--user">
			<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
				<span class="kt-hidden kt-header__topbar-welcome">Hi,</span>
				<span class="kt-hidden kt-header__topbar-username"><?php echo userinfo('name'); ?></span>
				<img class="kt-hidden" alt="Pic" src="<?php echo base_url(); ?>assets/media/users/300_21.jpg" />
				<span class="kt-header__topbar-icon kt-hidden-"><i class="flaticon2-user-outline-symbol"></i></span>
			</div>
			<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
				<!--begin: Navigation -->
				<div class="kt-notification">
					<a href="#" class="kt-notification__item">
						<div class="kt-notification__item-icon">
							<i class="flaticon2-calendar-3 kt-font-success"></i>
						</div>
						<div class="kt-notification__item-details">
							<div class="kt-notification__item-title kt-font-bold">
								My Profile
							</div>
							<div class="kt-notification__item-time">
								Account settings and more
							</div>
						</div>
					</a>
					<a href="#" class="kt-notification__item">
						<div class="kt-notification__item-icon">
							<i class="flaticon2-mail kt-font-warning"></i>
						</div>
						<div class="kt-notification__item-details">
							<div class="kt-notification__item-title kt-font-bold">
								My Messages
							</div>
							<div class="kt-notification__item-time">
								Inbox and tasks
							</div>
						</div>
					</a>
					<a href="#" class="kt-notification__item">
						<div class="kt-notification__item-icon">
							<i class="flaticon2-rocket-1 kt-font-danger"></i>
						</div>
						<div class="kt-notification__item-details">
							<div class="kt-notification__item-title kt-font-bold">
								My Activities
							</div>
							<div class="kt-notification__item-time">
								Logs and notifications
							</div>
						</div>
					</a>
					<a href="#" class="kt-notification__item">
						<div class="kt-notification__item-icon">
							<i class="flaticon2-hourglass kt-font-brand"></i>
						</div>
						<div class="kt-notification__item-details">
							<div class="kt-notification__item-title kt-font-bold">
								My Tasks
							</div>
							<div class="kt-notification__item-time">
								latest tasks and projects
							</div>
						</div>
					</a>
					<a href="#" class="kt-notification__item">
						<div class="kt-notification__item-icon">
							<i class="flaticon2-cardiogram kt-font-warning"></i>
						</div>
						<div class="kt-notification__item-details">
							<div class="kt-notification__item-title kt-font-bold">
								Billing
							</div>
							<div class="kt-notification__item-time">
								billing & statements <span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill kt-badge--rounded">2 pending</span>
							</div>
						</div>
					</a>
					<div class="kt-notification__custom kt-space-between">
						<a href="<?php echo site_url('auth/logout'); ?>" class="btn btn-label btn-label-brand btn-sm btn-bold">Sign Out</a>
						<a href="#" class="btn btn-clean btn-sm btn-bold">Upgrade Plan</a>
					</div>
				</div>

				<!--end: Navigation -->
			</div>
		</div>
	</div>
</div>