<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
* Jiwalu Framework
* A framework for PHP development
*
* @package     Jiwalu Framework
* @author      Jiwalu Studio
* @copyright   Copyright (c) 2019, Jiwalu Studio (https://www.jiwalu.id)
*/

require_once APPPATH."/third_party/Alibaba/autoload.php";
use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;

// Download：https://github.com/aliyun/openapi-sdk-php
// Usage：https://github.com/aliyun/openapi-sdk-php/blob/master/README.md

class SMS {

    protected $accessKeyId = 'LTAI4GKBzpvKnfXmapgFtDaN';
    protected $accessSecret = 'bJ1Zd6zPDXcY9BVaVnZx6bfGeh8cig';

    public function __construct() {
        parent::__construct();
    }

    public function send($phone, $message)
    {
        AlibabaCloud::accessKeyClient($this->accessKeyId, $this->accessSecret)
            ->regionId('ap-southeast-1')
            ->asDefaultClient();

        try {
            $result = AlibabaCloud::rpc()
                ->product('sms-intl')
                // ->scheme('https') // https | http
                ->version('2018-05-01')
                ->action('SendMessageToGlobe')
                ->method('POST')
                ->host('sms-intl.ap-southeast-1.aliyuncs.com')
                ->options([
                    'query' => [
                        'RegionId' => "ap-southeast-1",
                        'To' => "62".$phone,
                        'Message' => $message,
                    ],
                ])
                ->request();
            print_r($result->toArray());
        } catch (ClientException $e) {
            echo $e->getErrorMessage() . PHP_EOL;
        } catch (ServerException $e) {
            echo $e->getErrorMessage() . PHP_EOL;
        }
    }
}