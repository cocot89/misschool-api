<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
* Jiwalu Framework
* A framework for PHP development
*
* @package     Jiwalu Framework
* @author      Jiwalu Studio
* @copyright   Copyright (c) 2019, Jiwalu Studio (https://www.jiwalu.id)
*/

require_once APPPATH."/third_party/PHPExcel.php";

class Excel extends PHPExcel {

    public function __construct() {
        parent::__construct();
    }
       /**
     * @var CI_DB_result
     */
    private $array;
    private $column = array();
    private $header = array();
    private $width = array();
    private $header_bold = TRUE;
    private $start = 1;

    /**
     * Diisi dengan array Anda
     *
     * @access public
     * @param CI_DB_result $array
     * @return Excel_generator
     */
    public function set_array($array) {
        $this->array = $array;
        return $this;
    }

    /**
     * Diisi sesuai dengan field pada table
     * <pre>
     * $this->excel_generator->set_column(array('name', 'address', 'email'));
     * </pre>
     *
     * @access public
     * @param array $column
     * @return Excel_generator
     */
    public function set_column($column = array()) {
        $this->column = $column;
        return $this;
    }

    /**
     * Untuk mengisi header pada table excel
     * <pre>
     * $this->excel_generator->set_header(array('Name', 'Address', 'Email'));
     * </pre>
     * Jika ingin tulisannya tidak dalam bentuk bold
     * <pre>
     * $this->excel_generator->set_header(array('...'), FALSE);
     * </pre>
     *
     * @access public
     * @param array $header
     * @param bool $set_bold
     * @return Excel_generator
     */
    public function set_header($header = array(), $set_bold = TRUE) {
        $this->header = $header;
        $this->header_bold = $set_bold;
        return $this;
    }

    /**
     * Mengubah lebar kolom
     * <pre>
     * $this->excel_generator->set_width(array(25, 30, 15));
     * </pre>     *
     *
     * @access public
     * @param array $width
     * @return Excel_generator
     */
    public function set_width($width = array()) {
        $this->width = $width;
        return $this;
    }

    /**
     * Mengubah baris saat memulai membuat daftar
     * <pre>
     * $this->excel_generator->start_at(5);
     * </pre>
     *
     * @access public
     * @param int $start
     * @return Excel_generator
     */
    public function start_at($start = 1) {
        $this->start = $start;
        return $this;
    }

    /**
     * Untuk menghasilkan data excel
     *
     * @access public
     * @return Excel_generator
     */
    public function generate() {
        $start = $this->start;
        if (count($this->header) > 0) {
            $abj = 1;
            foreach ($this->header as $row) {
                $this->getActiveSheet()->setCellValue($this->columnName($abj) . $this->start, $row);
                if ($this->header_bold) {
                    $this->getActiveSheet()->getStyle($this->columnName($abj) . $this->start)->getFont()->setBold(TRUE);
                }
                $abj++;
            }
            $start = $this->start + 1;
        }

        foreach ($this->array as $result) {
            $index = 1;
            foreach ($this->column as $row) {
                if (count($this->width) > 0) {
                    $this->getActiveSheet()->getColumnDimension($this->columnName($index))->setWidth($this->width[$index - 1]);
                }

                $this->getActiveSheet()->setCellValue($this->columnName($index) . $start, $result[$row]);
                $index++;
            }
            $start++;
        }
        return $this;
    }

    private function columnName($index) {
        --$index;
        if ($index >= 0 && $index < 26)
            return chr(ord('A') + $index);
        else if ($index > 25)
            return ($this->columnName($index / 26)) . ($this->columnName($index % 26 + 1));
        else
            show_error("Invalid Column # " . ($index + 1));
    }

    /**
     * Untuk membuat file excel
     *
     * @param string $filename
     * @param string $writerType
     * @param string $mimes
     */
    private function writeToFile($filename = 'doc', $writerType = 'Excel5', $mimes = 'application/vnd.ms-excel') {
        $this->generate();
        header("Content-Type: $mimes");
        header("Content-Disposition: attachment;filename=\"$filename\"");
        header("Cache-Control: max-age=0");
        $objWriter = PHPExcel_IOFactory::createWriter($this, $writerType);
        $objWriter->save('php://output');
    }

    /**
     * @param string $filename
     */
    public function exportTo2003($filename = 'doc') {
        $this->writeToFile($filename . '.xls');
    }

    /**
     * @param string $filename
     */
    public function exportTo2007($filename = 'doc') {
        $this->writeToFile($filename . '.xlsx', 'Excel2007', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    }

}
