<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * CodeIgniter Rest Controller
 *
 * @version         1.0.0
 */
class RestController extends CI_Controller
{
    protected $request = null;
    protected $response = null;
    protected $rest = null;
    protected $allowed_http_methods = ['get', 'delete', 'post', 'put', 'options'];
    protected $_supported_formats = [
        'json'       => 'application/json',
        'array'      => 'application/json',
        'csv'        => 'application/csv',
        'html'       => 'text/html',
        'jsonp'      => 'application/javascript',
        'php'        => 'text/plain',
        'serialized' => 'application/vnd.php.serialized',
        'xml'        => 'application/xml',
    ];

    /**
     * Common HTTP status codes and their respective description.
     *
     * @link http://www.restapitutorial.com/httpstatuscodes.html
     */
    const HTTP_OK = 200;
    const HTTP_CREATED = 201;
    const HTTP_NOT_MODIFIED = 304;
    const HTTP_BAD_REQUEST = 400;
    const HTTP_UNAUTHORIZED = 401;
    const HTTP_FORBIDDEN = 403;
    const HTTP_NOT_FOUND = 404;
    const HTTP_METHOD_NOT_ALLOWED = 405;
    const HTTP_NOT_ACCEPTABLE = 406;
    const HTTP_INTERNAL_ERROR = 500;

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: Origin,X-Requested-With,Content-Type,Accept,Access-Control-Request-Method,Authorization,User-Id');
        header('Access-Control-Allow-Methods: GET,POST,OPTIONS,PUT,DELETE');
        header('Content-Type: application/json');

        // Initialise the response, request and rest objects
        $this->request = new stdClass();
        $this->response = new stdClass();
        $this->rest = new stdClass();

        // Determine whether the connection is HTTPS
        $this->request->ssl = is_https();

        // How is this request being made? GET, POST, PATCH, DELETE, INSERT, PUT, HEAD or OPTIONS
        $this->request->method = $this->_detect_method();

        if ($this->input->method() === 'options') {
            exit;
        }

        // // Only allow ajax requests
        // if ($this->input->is_ajax_request() === false) {
        //     // Display an error response
        //     $this->response([
        //         'status'  => false,
        //         'message' => $this->lang->line('text_rest_ajax_only'),
        //     ], 406);
        // }
    }


    /**
     * Requests are not made to methods directly, the request will be for
     * an "object". This simply maps the object and method to the correct
     * Controller method.
     *
     * @param string $object_called
     * @param array  $arguments     The arguments passed to the controller method
     *
     * @throws Exception
     */
    public function _remap($object_called, $arguments = [])
    {
        // Should we answer if not over SSL?
        // if ($this->config->item('force_https') && $this->request->ssl === false) {
        //     $this->response([
        //         'status'  => false,
        //         'message' => $this->lang->line('text_rest_unsupported'),
        //     ], HTTP_FORBIDDEN);
        // }

        // Remove the supported format from the function name e.g. index.json => index
        $object_called = preg_replace('/^(.*)\.(?:'.implode('|', array_keys($this->_supported_formats)).')$/', '$1', $object_called);

        $controller_method = $object_called.'_'.$this->request->method;
        // Does this method exist? If not, try executing an index method
        if (!method_exists($this, $controller_method)) {
            $controller_method = 'index_'.$this->request->method;
            array_unshift($arguments, $object_called);
        }

        // Sure it exists, but can they do anything with it?
        if (!method_exists($this, $controller_method)) {
            $this->response([
                'status'  => false,
                'message' => $this->lang->line('text_rest_unknown_method'),
            ], 405);
        }

        // Call the controller method and passed arguments
        try {
            call_user_func_array([$this, $controller_method], $arguments);
        } catch (Exception $ex) {
            if ($this->config->item('rest_handle_exceptions') === false) {
                throw $ex;
            }

            // If the method doesn't exist, then the error will be caught and an error response shown
            $_error = &load_class('Exceptions', 'core');
            $_error->show_exception($ex);
        }
    }

    /**
     * Takes mixed data and optionally a status code, then creates the response.
     *
     * @param array|null $data      Data to output to the user
     * @param int|null   $http_code HTTP status code
     * @param bool       $continue  TRUE to flush the response to the client and continue
     *                              running the script; otherwise, exit
     */
    public function response($data = null, $http_code = null, $continue = false)
    {
        ob_start();
        // If the HTTP status is not NULL, then cast as an integer
        if ($http_code !== null) {
            // So as to be safe later on in the process
            $http_code = (int) $http_code;
        }

        // Set the output as NULL by default
        $output = null;

        // If data is NULL and no HTTP status code provided, then display, error and exit
        if ($data === null && $http_code === null) {
            $http_code = 404;
        }

        // If data is not NULL and a HTTP status code provided, then continue
        elseif ($data !== null) {
            // If an array or object, then parse as a json, so as to be a 'string'
            if (is_array($data) || is_object($data)) {
                $data = Format::factory($data)->{'to_json'}();
            }

            // Format is not supported, so output the raw data as a string
            $output = $data;
        }

        // If not greater than zero, then set the HTTP status code as 200 by default
        // Though perhaps 500 should be set instead, for the developer not passing a
        // correct HTTP status code
        $http_code > 0 || $http_code = 200;

        $this->output->set_status_header($http_code);

        // Output the data
        $this->output->set_output($output);

        if ($continue === false) {
            // Display the data and exit execution
            $this->output->_display();
            exit;
        } else {
            if (is_callable('fastcgi_finish_request')) {
                // Terminates connection and returns response to client on PHP-FPM.
                $this->output->_display();
                ob_end_flush();
                fastcgi_finish_request();
                ignore_user_abort(true);
            } else {
                // Legacy compatibility.
                ob_end_flush();
            }
        }
        ob_end_flush();
    }

    /**
     * Takes mixed data and optionally a status code, then creates the response
     * within the buffers of the Output class. The response is sent to the client
     * lately by the framework, after the current controller's method termination.
     * All the hooks after the controller's method termination are executable.
     *
     * @param array|null $data      Data to output to the user
     * @param int|null   $http_code HTTP status code
     */
    public function set_response($data = null, $http_code = null)
    {
        $this->response($data, $http_code, true);
    }

    /**
     * Get the HTTP request string e.g. get or post.
     *
     * @return string|null Supported request method as a lowercase string; otherwise, NULL if not supported
     */
    protected function _detect_method()
    {
        // Declare a variable to store the method
        $method = null;

        // Determine whether the 'enable_emulate_request' setting is enabled
        $method = $this->input->post('_method');
        if ($method === null) {
            $method = $this->input->server('HTTP_X_HTTP_METHOD_OVERRIDE');
        }

        $method = strtolower($method);

        if (empty($method)) {
            // Get the request method as a lowercase string
            $method = $this->input->method();
        }

        return in_array($method, $this->allowed_http_methods) ? $method : 'get';
    }

    public function validate_user_token()
    {
        // $this->load->model('user/member_tokens');
        $token = $this->input->get_request_header('Authorization');
        $user_id = $this->input->get_request_header('User-Id');
        $user_id = decode($user_id);
        $user_id = $user_id ? $user_id : 0;
        $this->user = $this->user_tokens->check_token($user_id, $token);
        if (!$this->user) {
            $this->response([
                'status' => false,
                'message' => 'User not authorized'
            ], 401);
        }
    }

}